const express = require("express");
const app = express();
const path = require("path");

app.use("assets", express.static(path.join(__dirname, "assets")));
app.use(express.static(__dirname));

app.get("/", function (req, res) {
  res.render(__dirname + "/tampilan.ejs");
});

app.get("/game", function (req, res) {
  res.render(__dirname + "/game.ejs");
});

app.get("/login", function (req, res) {
  res.render(__dirname + "/login.ejs");
});

app.listen(4000, () => {
  console.log("server nyala di http://localhost:4000");
});
